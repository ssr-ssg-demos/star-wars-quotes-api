# Star Wars Quotes API

This is a Hasura-based API for managing Star Wars quotes.

## Getting Started

Install Docker and run the following command:

```bash
$ docker-compose up
```

Navigate to http://localhost:8080 to view and interact with the Hasura console.
